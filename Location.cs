using System;

public class Location{
  public string Name;
  public string Description;
  public Location North;
  public Location South;
  public Location East;
  public Location West;
  public bool locked;
  public bool hasKey;
  public bool hasSword;
  public bool hasScroll;
  public bool hasBook;
  public bool noEnemy;
  public bool hasDoor;
  public bool hasGrave;
  public bool hasNPC;
  public bool hasBoss;
  public bool hasBookshelf;




//CONSTRUCTOR
  public Location (string name,string description){
    this.Name = name;
    this.Description = description;
    this.locked = false;
    this.hasKey = false;
    this.hasSword = false;
    this.hasScroll = false;
    this.hasBook = false;
    this.hasDoor = false;
    this.hasGrave = false;
    this.hasNPC = false;
    this.noEnemy = true;
    this.hasBoss = false;
    this.hasBookshelf = false;
  }

//get description command
  public string GetDescription(){
    string gameplayDescription = "";

    gameplayDescription += "\n" + "*** " + this.Name + " ***" + "\n";
    gameplayDescription += this.Description + "\n" + "\n";

    if (this.North != null){
      gameplayDescription += "to the north you see: " + this.North.Name + "\n";
    }

    if (this.South != null){
      gameplayDescription += "to the south you see: " + this.South.Name + "\n";
    }

    if (this.East != null){
      gameplayDescription += "to the east you see: " + this.East.Name + "\n";
    }

    if (this.West != null){
      gameplayDescription += "to the west you see: " + this.West.Name + "\n";
    }
    return gameplayDescription;
  }

//links North East South West locations
  public void allLink(Location N,Location S,Location E,Location W){
    this.North = N;
    N.South = this;
    this.South = S;
    S.North = this;
    this.East = E;
    E.West = this;
    this.West = W;
    W.East = this;
  }

//links North South
  public void nsLink(Location N,Location S){
    this.North = N;
    N.South = this;
    this.South = S;
    S.North = this;
  }

//links East West
  public void ewLink(Location E,Location W){
    this.East = E;
    E.West = this;
    this.West = W;
    W.East = this;
  }



}
