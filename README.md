# Interactive Fiction

A very simple Interactive Fiction game based on Zork

List of commands:

go ____

attack ____

use ____

dig

talk

yes

no

grab ____

drop ____

inventory

firebolt _____


Note that while examining objects is implemented, there are not very many objects that can be examined. Any objects that can/should be examined are hinted at in the text of the game.
For testing purposes, the objects that can be examined are:
1. the door at HouseExt
2. the gravestone at ExtEast
3. the bookshelf at HouseBaseW

