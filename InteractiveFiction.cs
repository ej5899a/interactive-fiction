using System;
using System.Collections.Generic;

public class InteractiveFiction{
  static void Main(string[] args) {
    PlayGame();
  }

  public static void PlayGame() {


    //create game world
    Location Cliff = new Location("The Seaside Cliff","You hear the waves crashing against the base of the cliff. To the horizon you see the sun shining down onto the glistening sea.");
    Location Field = new Location("Unknown Open Field","Not much is here, but you hear crying in the distance. ");
    Location ExtWest = new Location("A Forest","The Forest is very dense with plants, the distant crying gets louder");
    Location ExtEast = new Location("An Old Graveyard","What a sad place. The area has been cleared of grass and has a large gravestone in the center.");
    Location ExtWest2 = new Location("Enchanted Forest","What on Earth? You head deeper into the forest only to find yourself surrounded by strange plants and flying creatures. The air is cluttered with luminescent particles that light up an otherwise dark area, as the sun is blocked by the thick foliage. In the center you see a clearing where a woman is on her knees crying. Perhaps you should try talking to her.");
    Location HouseExt = new Location("Mysterious House","You walk up towards a mysterious house. The walls are made of stone and are covered in moss. There is a door, but you do not see any windows.");
    Location House = new Location("House Interior","You successfully enter the house. The air inside is cold and damp. A negative aura in the air sends chills down your spine. The room is dark, but you see a dim light emitting from the Western wing of the house. To the East all you see is darkness, you can't go there without light.");
    Location HouseW = new Location("House: Western Wing","You go towards the Western wing and notice that there is a glowing scroll on the floor. That must have been the source of the light! You go to grab it, but are stopped by a large shadowy figure. It's a goblin! What do you do?");
    Location HouseE = new Location("House: Eastern Wing","You go into the Eastern wing of the house. You wisp familiar lights up the way from here.");
    Location HouseBaseE = new Location("House Basement: East","You head down the stairs towards the basement. At the bottom, you see a spooky skeleton guarding the doorway. You must defeat it to progress.");
    Location HouseBase = new Location("House Basement: Central Corridor","You head to the Central Corridor. Here you can feel a strong negative energy emitting from the door to your North. To you West you see something interesting...");
    Location HouseBaseW = new Location("House Basement: West","The room is filled with bookshelves. It seems that this room is some sort of library. Perhaps you can find something useful here by examining the bookshelf.");
    Location BossRoom = new Location("Lair of the Ancients","You enter the room and immediately begin to feel the negative aura draining the heat energy from your body. At the end of the room is a Frost Lich. You must defeat it.");

    //creates NPC
    NPC sylvia = new NPC(">Oh hello, my name is Sylvia... Why am I crying?... My family and I used to live at the house back North East, but an evil Frost Lich and his minions rampaged our home and killed everyone except me. It would put me at ease to see justice be brought to the Frost Lich. Will you help me?", ">Thank you so much! I will forever be in your debt. There should be a key to the house hidden in the Graveyard, just dig there. Here take this, it might help you.", ">Please... you're my only hope.");



    //spawns items in map and places other objects
    ExtEast.hasKey = true;
    ExtWest2.hasNPC = true;
    HouseW.hasScroll = true;
    HouseW.noEnemy = false;
    HouseBaseW.hasBook = true;
    HouseExt.hasDoor = true;
    HouseBaseE.noEnemy = false;
    ExtEast.hasGrave = true;
    BossRoom.hasBoss = true;
    House.locked = true;
    HouseE.locked = true;
    HouseBase.locked = true;
    HouseBaseW.hasBookshelf = true;





    //link locations
    Cliff.North = Field;
    Field.allLink(HouseExt,Cliff,ExtEast,ExtWest);
    ExtWest.West = ExtWest2;
    ExtWest2.East = ExtWest;
    HouseExt.North = House;
    House.South = HouseExt;
    House.ewLink(HouseE,HouseW);
    HouseE.North = HouseBaseE;
    HouseBaseE.South = HouseE;
    HouseBase.ewLink(HouseBaseE,HouseBaseW);
    HouseBase.North = BossRoom;
    BossRoom.South = HouseBase;


    //create INVENTORY
    List<Item> inventory = new List<Item>();
    Item key = new Item("Key");
    Item sword = new Item("Sword");
    Item scroll = new Item("Spell Scroll");
    Item book = new Item("Spell Book");



    //sets start point
    Location currentLocation;
    currentLocation = Field;

    //starter script
    Console.WriteLine("You awaken in a strange place with no memory of how you ended up unconscious..." + "\n");




    //game loop
    bool playing = true;
    while(playing){

      //print the description of current Location
      string description = currentLocation.GetDescription();
      Console.WriteLine(description);

      //get a command
      Console.Write("> ");
      string command = Console.ReadLine();

      //process command

        // Split commands seperates strings into arrays and sperates them line by line based on space
        // seperates each command into verb/noun
      string[] splitCommands = command.Split(' ');

      // move command
      if (splitCommands[0].ToLower() == "go") {
        if (splitCommands[1].ToLower() == "north" && currentLocation.North != null  && currentLocation.North.locked != true){
          currentLocation = currentLocation.North;


        }else if (splitCommands[1].ToLower() == "south" && currentLocation.South != null  && currentLocation.South.locked != true){
          currentLocation = currentLocation.South;


        }else if (splitCommands[1].ToLower() == "east" && currentLocation.East != null  && currentLocation.East.locked != true){
          currentLocation = currentLocation.East;


        }else if (splitCommands[1].ToLower() == "west" && currentLocation.West != null  && currentLocation.West.locked != true){
          currentLocation = currentLocation.West;


        }else{
          Console.WriteLine("You tried to go " + splitCommands[1] + " but you can't" );
        }
      }

      //check inventory command
      if (splitCommands[0].ToLower() == "inventory") {
        Console.WriteLine("\n"+"--- INVENTORY ---");
        //Loop through everything in the inventory and print out its name
        foreach(Item item in inventory) {
          Console.WriteLine("\t" + item.Name);
        }
      }

      //grab command
      //once an item is grabbed it will be marked as being in your inventory;
      if (splitCommands[0].ToLower() == "grab") {
        if (splitCommands[1].ToLower() == "key" && currentLocation.hasKey == true){
          inventory.Add(key);
          key.inInventory = true;
          currentLocation.hasKey = false;
          Console.WriteLine(key.Name + " added");
        }else if (splitCommands[1].ToLower() == "sword" && currentLocation.hasSword == true){
          inventory.Add(sword);
          sword.inInventory = true;
          currentLocation.hasSword = false;
          Console.WriteLine(sword.Name + " added");
        }else if (splitCommands[1].ToLower() == "scroll" && currentLocation.hasScroll == true){
          if (currentLocation.noEnemy == true) { // prevents you from grabbing scroll when enemy is in room
            inventory.Add(scroll);
            scroll.inInventory = true;
            currentLocation.hasScroll = false;
            Console.WriteLine(scroll.Name + " added");
          }else{
              Console.WriteLine("\n" + "*You must kill the enemy goblin before trying to get the scroll!");
          }
        }else if (splitCommands[1].ToLower() == "book" && currentLocation.hasBook == true){
          inventory.Add(book);
          book.inInventory = true;
          currentLocation.hasBook = false;
          Console.WriteLine(book.Name + " added");
        }else{
          Console.WriteLine("\n" + "there is no " + splitCommands[1] +" here to grab");
        }
      }


      //drop command
      if (splitCommands[0].ToLower() == "drop") {
        if (splitCommands[1].ToLower() == "key" && key.inInventory == true){
          inventory.Remove(key);
          key.inInventory = false;
          currentLocation.hasKey = true;
          Console.WriteLine(key.Name + " dropped");
        }else if (splitCommands[1].ToLower() == "sword" && sword.inInventory == true){
          inventory.Remove(sword);
          sword.inInventory = false;
          sword.used = false; // sets sword use to false to disable combat ability once dropped
          currentLocation.hasSword = true;
          Console.WriteLine(sword.Name + " dropped");
        }else if (splitCommands[1].ToLower() == "scroll" && scroll.inInventory == true){
          inventory.Remove(scroll);
          scroll.inInventory = false;
          currentLocation.hasScroll = true;
          Console.WriteLine(scroll.Name + " dropped");
        }else if (splitCommands[1].ToLower() == "book" && book.inInventory == true){
          inventory.Remove(book);
          book.inInventory = false;
          currentLocation.hasBook = true;
          Console.WriteLine(book.Name + " dropped");
        }else{
          Console.WriteLine("\n" + "there is no " + splitCommands[1] +" in your inventory to drop");
        }
      }

      //use command
      if (splitCommands[0].ToLower() == "use") {
        if (splitCommands[1].ToLower() == "key" && key.inInventory == true && currentLocation == HouseExt){
          inventory.Remove(key);
          key.inInventory = false;
          key.used = true;
          Console.WriteLine(key.Name + " used. The door is open.");
        }else if (splitCommands[1].ToLower() == "sword" && sword.inInventory == true){  // if you use the sword it shouldn't remove it from your inventory. When you drop it, it will mark sword as unused however.
          sword.used = true;
          Console.WriteLine("\n" + "You equipped the sword! You may now use the attack _______ command");
        }else if (splitCommands[1].ToLower() == "scroll" && scroll.inInventory == true){
          inventory.Remove(scroll);
          scroll.inInventory = false;
          scroll.used = true;
          Console.WriteLine("\n" + "You read the scroll, casting a magic spell that summons a light wisp to guide you through the dark. You may now venture into dark areas" + "\n" + "The scroll evaporates..");
        }else if (splitCommands[1].ToLower() == "book" && book.inInventory == true){
          book.used = true;
          Console.WriteLine("\n" + "You read the ancient book and learn the firebolt spell! Use the command firebolt ______ to cast towards enemies!");
        }else{
          Console.WriteLine("\n" + "You cannot use " + splitCommands[1] +" here or it is not in your inventory to use");
        }
      }

      //attack command - can only attack given that sword is in use
      if (sword.used == true){
        if (splitCommands[0].ToLower() == "attack") {
          if (splitCommands[1].ToLower() == "goblin" && currentLocation.noEnemy == false){
            currentLocation.noEnemy = true;
            Console.WriteLine("\n" + "You have slain the goblin before you!");
          }else if (splitCommands[1].ToLower() == "lich" && currentLocation.hasBoss == true){
            Console.WriteLine("\n" +"You swung your sword at the Frost Lich, but it is unfazed by your actions. Perhaps it has a different weakness.");
          }else if (splitCommands[1].ToLower() == "sylvia" && currentLocation.hasNPC){
            Console.WriteLine("\n" +"NO, STOP IT. DON'T DO THAT!");
          }else if (splitCommands[1].ToLower() == "skeleton" && currentLocation.noEnemy == false){
            currentLocation.noEnemy = true;
            Console.WriteLine("\n" + "You have slain the skeleton before you, clearing the way westward.");
          }else{
            Console.WriteLine("\n" +"There isn't anything like that around to attack.");
          }
        }
      }

      //if the sword is not in use
      if (sword.used == false){
        if (splitCommands[0].ToLower() == "attack"){
            Console.WriteLine("\n" +"You have no weapon to attack with!");
        }
      }

      //firebolt command and game ender

      if (splitCommands[0].ToLower() == "firebolt" && book.used == true) {
        if (splitCommands[1].ToLower() == "lich" && currentLocation.hasBoss == true){
          currentLocation.hasBoss = false;
          Console.WriteLine("\n" + "You fired a firebolt at the Lich and it disintegrated!");
       }else{
          Console.WriteLine("\n" +"There is nothing like that around to firebolt.");
        }
      }


      //examine commands
      if (splitCommands[0].ToLower() == "examine"){
        if (splitCommands[1].ToLower() == "grave" || splitCommands[1].ToLower() == "gravestone" && currentLocation.hasGrave) {
          Console.WriteLine("\n" + "This gravestone looks old... It has been here for quiet a while. The soil around it is soft enough to dig.");
        }else if (splitCommands[1].ToLower() == "door" && currentLocation.hasDoor == true) {
          if (currentLocation.North.locked == true){
            Console.WriteLine("This door is locked shut and is sturdy. The only way to get through is with a key.");
          }else{
            Console.WriteLine("It's open");
          }
        }else if (splitCommands[1].ToLower() == "bookshelf" && currentLocation.hasBookshelf == true){
          Console.WriteLine("\n" + "You notice a book labeled *Pyromancy* among the other books in the shelf. Maybe this will be useful.");
        }else{
          Console.WriteLine("\n" + "Nothing much to say...");
        }


      }

      //talk commands
      if (splitCommands[0].ToLower() == "talk" && currentLocation.hasNPC){
        Console.WriteLine("\n" + "*You ask if the woman is alright*");
        Console.WriteLine("\n" + sylvia.DialogueA);
      }

      //dialogue responses
      if (splitCommands[0].ToLower() == "yes" && currentLocation.hasNPC){
        Console.WriteLine("\n" + sylvia.DialogueY);
        Console.WriteLine("\n" + "*Sylvia hands you a sword*");
        inventory.Add(sword);
        sword.inInventory = true;
        Console.WriteLine("\n" + "*Sword added to inventory*");
      }else if(splitCommands[0].ToLower() == "no" && currentLocation.hasNPC){
          Console.WriteLine("\n" + sylvia.DialogueN);
          Console.WriteLine("\n" + "*It doesn't seem you have much of a choice... say yes*");
      }

      //dig command
      if (splitCommands[0] == "dig" && currentLocation == ExtEast){
        Console.WriteLine("\n" + "You dug a hole under the grave, revealing a key to the house.");
      }else if(splitCommands[0] == "dig" && currentLocation != ExtEast){
        Console.WriteLine("\n" + "None of the soil here looks soft enough to dig up.");
      }

      //scripted conditions for progression
      if (key.used == true){
        House.locked = false;

      }

      if (scroll.used == true){
        HouseE.locked = false;
      }

      if (HouseBaseE.noEnemy == true){
        HouseBase.locked = false;
      }

      //exit command
      if (command.ToLower() == "exit"){
        playing = false;
      }

      //game completion condition
      if (BossRoom.hasBoss == false){
        Console.WriteLine ("\n" + "Congradulations! You have completed Sylvia's request and the game! Thanks for playing!");
        playing = false;
      }




    }
  }
}
