using System;

public class NPC {
  public string DialogueA;
  public string DialogueY;
  public string DialogueN;
  public bool questAccepted;

  public NPC(string a, string y, string n){
    this.DialogueA = a;
    this.DialogueY = y;
    this.DialogueN = n;
    this.questAccepted = false;

  }


}
